(function ($) {

  Drupal.behaviors.entityreferenceIcons = {
    attach: function (context) {
      $(".field-widget-entityreference-icons-select select", context).once("eri-processed", function () {
        var field = $(this).attr('data-field-name');
        var field_id = $(this).attr('id');
        var field_name = $(this).attr('name');
        var icons = Drupal.settings.entityreferenceIcons[field].icons;

        // Prepare selectbox for plugin
        $(this).find("option[value!=_none]").each(function () {
          var icon = icons[$(this).val()];

          $(this).attr('data-imagesrc', icon ? icon : icons["default"]);
        });

        // Activate plugin
        $(this).ddslick({
          onSelected: function (data) {
            $('#' + field_id + ' input', context).val(data.selectedData.value);
          }
        });

        // Set input field name
        $('#' + field_id + ' input', context).attr("name", field_name);
      });
    }
  };

})(jQuery);
